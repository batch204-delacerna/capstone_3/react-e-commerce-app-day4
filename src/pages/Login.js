import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Redirect } from 'react-router-dom';
// import { AiOutlineEyeInvisible, AiOutlineEye } from 'react-icons';

export default function Login(props) {
  
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);
  
  const { user, setUser } = useContext(UserContext);

  // Initialize a boolean state
  // const [passwordShown, setPasswordShown] = useState(false);


  // Password toggle handler
  const [passwordType, setPasswordType] = useState("password");

  const togglePassword = () => {
    if(passwordType==="password")
      {
        //<AiOutlineEye/>
        setPasswordType("text")
        return;
      }
    //<AiOutlineEyeInvisible/>
    setPasswordType("password");
  };


  useEffect(() => {
    if((email !== '' && password !== '')) {
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [email, password]);

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/credentials`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
    })
  }

  function loginUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(res => res.json())
    .then(data => {
      // console.log(data);
      if (typeof data.access !== "undefined") {
        localStorage.setItem("token", data.access);
        retrieveUserDetails(data.access);

        alert("Successfully logged in.")
        props.history.push("/products");
      } else {
        alert("Login failed. Please try again.");
        setEmail("");
        setPassword("");
      }
    })

  }

  return (
      (!user.id) ?
      <Form onSubmit={e => loginUser(e)}>
          <Form.Group controlId="userEmail">
            <Form.Label>Email Address</Form.Label>
            <Form.Control
              type="email"
              placeholder = "Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="password">
            <Form.Label className="mt-3">Password</Form.Label>
            <Form.Control
              type={passwordType}
              placeholder = "Enter Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              onClick={togglePassword}
              input=""
            />
          </Form.Group>

          {isActive ?
            <Button className="mt-3" variant="primary" type="submit" id="submitBtn">
              Login
            </Button>
            :
            <Button className="mt-3" variant="primary" id="submitBtn" disabled>
              Login
            </Button>
          }

      </Form>
      :
      <Redirect to="/"/>
  );

}