import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {
	
	const data = {
		title: "MIKKI THE GREAT STORE",
		content: "Bili na kayo mga suki, mura lang. Presyong Divisoria.",
		destination: "/products",
		label: "See shop now!"
	}

	return(
		<>
			<Banner dataProp={data} />
			<Highlights />
		</>
	)
}