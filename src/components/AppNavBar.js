import { useContext } from 'react';
import { Button, Container, Form, Nav, Navbar, Offcanvas } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

import '../App.css';

export default function AppNavBar() {

	const { user } = useContext(UserContext);

	return(
		<Navbar key="md" bg="success" expand="md" className="mb-3">
		  <Container fluid>
		    <Navbar.Brand as={Link} to="/" className="p-5">MDG</Navbar.Brand>
		    <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-md`} />
		    <Navbar.Offcanvas
		      id={`offcanvasNavbar-expand-md`}
		      aria-labelledby={`offcanvasNavbarLabel-expand-md`}
		      placement="end"
		    >
		      <Offcanvas.Header closeButton>
		        <Offcanvas.Title id={`offcanvasNavbarLabel-expand-md`}>
		          MDG
		        </Offcanvas.Title>
		      </Offcanvas.Header>
		      <Offcanvas.Body>
		        <Nav className="justify-content-end flex-grow-1 pe-3">
		          <Nav.Link as={Link} to="/">Home</Nav.Link>
		          <Nav.Link as={Link} to="/products">Products</Nav.Link>
		          {(!user.id) ?
		          		<>
				            <Link className="nav-link" to="/login">Login</Link>
				            <Link className="nav-link" to="/register">Register</Link>
			            </>
			            :
			            (!user.isAdmin) ?
			            	<>
				            <Link className="nav-link" to="/cart">Cart</Link>
				            <Link className="nav-link" to="/logout">Logout</Link>
			            	</>
			            	:
			            	<Link className="nav-link" to="/logout">Logout</Link>
			        	
		        	}
		        </Nav>
					<Form className="d-flex">
						<Form.Control
							type="search"
							placeholder="Search"
							className="me-2"
							aria-label="Search"
						/>
						<Button variant="outline-success">Search</Button>
					</Form>
		      </Offcanvas.Body>
		    </Navbar.Offcanvas>
		  </Container>
		</Navbar>
	);
}